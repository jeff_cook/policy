## [0.7.1](https://gitlab.com/jeff_cook/policy/compare/v0.7.0...v0.7.1) (2021-04-25)


### Bug Fixes

* **grype:** use alpine and install grype ([5f21397](https://gitlab.com/jeff_cook/policy/commit/5f213978eb6c0cb9ff32c841522cace39eecfe3e))
* **grype:** use pre-built image ([8ce529d](https://gitlab.com/jeff_cook/policy/commit/8ce529d643a6e8821a6f6793aa1bfd212cd98033))

# [0.7.0](https://gitlab.com/jeff_cook/policy/compare/v0.6.2...v0.7.0) (2021-02-20)


### Features

* add reviewdog to shellcheck ([b592622](https://gitlab.com/jeff_cook/policy/commit/b59262211b3735d933a71106d99d7b1bdaaa9b84))

## [0.6.2](https://gitlab.com/jeff_cook/policy/compare/v0.6.1...v0.6.2) (2021-02-16)


### Bug Fixes

* only run semantic-release if there is a config file ([31bc078](https://gitlab.com/jeff_cook/policy/commit/31bc0784115668d1299f18d4232e5dff7c2a8d8f))

## [0.6.1](https://gitlab.com/jeff_cook/policy/compare/v0.6.0...v0.6.1) (2020-10-28)


### Bug Fixes

* use slug for branch tags and name for releases ([4f63cf0](https://gitlab.com/jeff_cook/policy/commit/4f63cf0249d3230ca169324cd1edf59cb358e4c5))

# [0.6.0](https://gitlab.com/jeff_cook/policy/compare/v0.5.2...v0.6.0) (2020-10-28)


### Bug Fixes

* use hadolint.txt file ([a16e6b4](https://gitlab.com/jeff_cook/policy/commit/a16e6b473154e39366a18dc31594bf2c5957792a))


### Features

* add hadolint to reviewdog ([1d5cc72](https://gitlab.com/jeff_cook/policy/commit/1d5cc72f30d59bd6c2f490c54d5884f0c0e42dd2))
* save text file ([f732a28](https://gitlab.com/jeff_cook/policy/commit/f732a28a64f1c0dc1e19b71bfa50208324251a38))

## [0.5.2](https://gitlab.com/jeff_cook/policy/compare/v0.5.1...v0.5.2) (2020-10-26)


### Bug Fixes

* warn if there is no config file ([d95f2e9](https://gitlab.com/jeff_cook/policy/commit/d95f2e988f2f46dbc2a0ef0a6f43419296a06fbf))

## [0.5.1](https://gitlab.com/jeff_cook/policy/compare/v0.5.0...v0.5.1) (2020-10-24)


### Bug Fixes

* artifact paths ([ffcb0ee](https://gitlab.com/jeff_cook/policy/commit/ffcb0ee62adcc7d378085b0b4101749a7045a3e7))

# [0.5.0](https://gitlab.com/jeff_cook/policy/compare/v0.4.0...v0.5.0) (2020-10-24)


### Features

* trivy scan with full json output ([045c6ff](https://gitlab.com/jeff_cook/policy/commit/045c6ffa56fa20420fd55eee8e871203ad732be1))

# [0.4.0](https://gitlab.com/jeff_cook/policy/compare/v0.3.0...v0.4.0) (2020-10-24)


### Features

* add opencontainers labels to docker image ([b4dbc3c](https://gitlab.com/jeff_cook/policy/commit/b4dbc3c8eab4302e2b7f69b3dbf815d009766635))

# [0.3.0](https://gitlab.com/jeff_cook/policy/compare/v0.2.0...v0.3.0) (2020-10-22)


### Features

* scan image with dockle ([2499171](https://gitlab.com/jeff_cook/policy/commit/2499171b786b84923a6ddb500bbc7b1c0b795399))

# [0.2.0](https://gitlab.com/jeff_cook/policy/compare/v0.1.0...v0.2.0) (2020-10-22)


### Bug Fixes

* **deps:** update jeff_cook/trivy to v0.5.0 ([8548326](https://gitlab.com/jeff_cook/policy/commit/85483269c24a6a0b800ff5731e424722491aaf39))
* add dive to standard pipeline ([90b6bde](https://gitlab.com/jeff_cook/policy/commit/90b6bde57e20a2ed183b36ede6b609461cea84f1))
* add npm-audit to shared pipeline ([443ff4c](https://gitlab.com/jeff_cook/policy/commit/443ff4cb9ab36ee6b3f1fa8ef743d4bfafe27895))
* add shellcheck to reviewdog config ([e20e1f6](https://gitlab.com/jeff_cook/policy/commit/e20e1f694792b630cea1a9166a881e5c28d2e41d))
* allow Dockerfile in diff dir ([091c8a4](https://gitlab.com/jeff_cook/policy/commit/091c8a43a229953c834da164f57414e7925f7346))
* allow failure ([d02572c](https://gitlab.com/jeff_cook/policy/commit/d02572c588b9e5ef18cdea9e330a18a758153baa))
* allow failure ([6c4f0b3](https://gitlab.com/jeff_cook/policy/commit/6c4f0b35ff431e0f0d57e4771c746791845966e9))
* do not run if no Dockerfile ([9549cf3](https://gitlab.com/jeff_cook/policy/commit/9549cf31f7b261b24537826f727d2eb1c35bca1d))
* move gitleaks to .pre stage ([c4da254](https://gitlab.com/jeff_cook/policy/commit/c4da25489ba013d6226587f1206326a7ac5a3503))
* remove microscanner ([ac91b08](https://gitlab.com/jeff_cook/policy/commit/ac91b08328aeab6a0dd06ca9093dd9912e854c4e))
* remove microscanner ([fec6bb6](https://gitlab.com/jeff_cook/policy/commit/fec6bb6c2fe6dabdb8f8a3f99feaa3938a76eb5a))
* review diff with source branch ([91b3b31](https://gitlab.com/jeff_cook/policy/commit/91b3b31d5aa9d18b8aba4076be55502ec42d2730))
* save artifact ([52f4e84](https://gitlab.com/jeff_cook/policy/commit/52f4e840a2f7ebe18df7122477feacf5c343f0e6))
* skip if no dockerfile ([df45ec7](https://gitlab.com/jeff_cook/policy/commit/df45ec7f61b0d210b3c350253ec2e47b6a39e597))
* **hadolint:** use DOCKER_BUILD_PATH var ([8f51ce2](https://gitlab.com/jeff_cook/policy/commit/8f51ce28641f5b68b1ee8029088546a1b0c9f9a2))
* always save artifacts ([2ffa866](https://gitlab.com/jeff_cook/policy/commit/2ffa866e4ef401a1d3973b860cd853d9195b4306))
* remove testing config ([82531bd](https://gitlab.com/jeff_cook/policy/commit/82531bd5fdad7691b5db624763685e5e82e9bd75))
* save shfmt report ([5cd87a9](https://gitlab.com/jeff_cook/policy/commit/5cd87a9055bb15a262d52500a743af881aefafd1))
* update config ([a477955](https://gitlab.com/jeff_cook/policy/commit/a477955b988bd353aeca61f7138fc7aa49582f1f))
* use var for dockerfile path ([5a6cd1b](https://gitlab.com/jeff_cook/policy/commit/5a6cd1bfe3f408b9c04b4569a42cf217174b34c6))
* var name ([211520e](https://gitlab.com/jeff_cook/policy/commit/211520e9f3c60d6aeee3b554366ab13cd7e99b1c))
* var usage ([fd06ce0](https://gitlab.com/jeff_cook/policy/commit/fd06ce08d79c32b4985b52d1ca1e10ef8fa3b000))


### Features

* add dive docker image tester ([2bc57d6](https://gitlab.com/jeff_cook/policy/commit/2bc57d6d1fda1a842e8d211b0c76f58ef5b76efb))
* add dockerfile_lint ([9174526](https://gitlab.com/jeff_cook/policy/commit/9174526748ba8d81db85e5c6d04ca3a75126f3e0))
* add gitleaks ([20dc3ed](https://gitlab.com/jeff_cook/policy/commit/20dc3ed2d1c141cd5d40d2c2a5d940fc8861a24a))
* add grype scan ([c3686d1](https://gitlab.com/jeff_cook/policy/commit/c3686d1059acd94a74eb17484334dba6ecd4be53))
* add hadolint ([5c1b240](https://gitlab.com/jeff_cook/policy/commit/5c1b240659b92924e36661a13df8f500512bf99b))
* add npm-audit ([05ecb5d](https://gitlab.com/jeff_cook/policy/commit/05ecb5da3d8ec07e1e4bc55bc8fcd2acbda097bf))
* add pylint ([1b063ce](https://gitlab.com/jeff_cook/policy/commit/1b063ce656361a0d90ba4cba1f850509632a9e82))
* add reviewdog ([e146318](https://gitlab.com/jeff_cook/policy/commit/e146318b6668ffc204646e078482131afd5b7087))
* add semantic-release ([5a7b651](https://gitlab.com/jeff_cook/policy/commit/5a7b651af370eb521bc5711534235145497767f7))
* add shellcheck ([1570d2c](https://gitlab.com/jeff_cook/policy/commit/1570d2c79d12a7d6bbba94b0115b4bbc667ac5c8))
* add shfmt ([5474877](https://gitlab.com/jeff_cook/policy/commit/5474877bb43bcfbb9c400d470298abd5774b5bd4))
* add sonarscanner ([5e9b7b9](https://gitlab.com/jeff_cook/policy/commit/5e9b7b9bf1247766a0ed93ec803657bc3388c2c7))
* add sonarscanner ([d7e9411](https://gitlab.com/jeff_cook/policy/commit/d7e94118a156809f20cc9e3c91ae5a0a1b3ade3b))
* add syft scan ([2fecbfe](https://gitlab.com/jeff_cook/policy/commit/2fecbfe05f909590bc09146b3ab42e41a1b4d121))
* report on any todos ([4130077](https://gitlab.com/jeff_cook/policy/commit/41300774dd8349946f926d57e79ee50b47a8cd77))
