# Dive

A tool for exploring a docker image, layer contents, and discovering ways to shrink the size of your Docker/OCI image.

- <https://github.com/wagoodman/dive>
- <https://medium.com/nexton/how-to-optimize-docker-images-using-dive-dc590f45dbf5>
